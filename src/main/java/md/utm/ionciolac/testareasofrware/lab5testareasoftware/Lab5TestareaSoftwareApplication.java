package md.utm.ionciolac.testareasofrware.lab5testareasoftware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab5TestareaSoftwareApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lab5TestareaSoftwareApplication.class, args);
    }

}
